# skin.pm3.hd

This is a modification of `skin.pm3.hd` that adds compatibility with Kodi Matrix/Nexus.`*`

## Additional changes
Planned: 
- New textures for everything, to modernize the UI.
- Proper support for settings levels
- Better touch support, on par with [Confluence](https://github.com/xbmc/skin.confluence)

Implemented:
- None yet    

`*` PM3HD is technically already functional on 19.4 (and likely 19.5 though this has not been confirmed) and (at least) the Alpha 2 release of Nexus. (Current versions of Nexus have not been tested as of yet.) The challenges lie in implementing things like settings levels and new features to make the skin usable by modern standards.

## Installation
**`¯\_(ツ)_/¯`** idk